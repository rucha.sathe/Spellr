#include <stdio.h>
#include "hashtable.h"
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#define dict "/usr/share/dict/american-english"
/* 
*  To create a hashtable of all the words in the dictionary. 
*/
void createTable(word** hth, word** htt) {
	int fd, i = 0;
	char ch;
	char line[32];
	fd = open(dict, O_RDONLY);
	if(fd == -1) {
		printf("Error in opening file.");
		exit(0);
	}
	while(read(fd, &ch, 1) != 0) {
		if(ch == '\n') {
			line[i] = '\0';
			insertWord(hth, htt, line);
			i = 0;
			strcpy(line, "");
		}
		else {
			line[i] = ch;
			i++;
		}
	}
	close(fd);
}
