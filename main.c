#include <stdio.h>
#include "hashtable.h"
#include "DL.h"
#include "replaceall.h"
#include "suggestions.h"
#include "create.h"
#include "interface.h"
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#define filename "aspbak123.txt"
char op[33];
int r = 0;
void fileop(FILE* fp1, word** hth, word** htt, word** plist, char* x) {
	word* pfin = NULL;
	word* psugg = NULL;
	char cur[33];
	word* suggh[5];
	word* suggt[5];
	init(suggh, suggt, 5);
	word* finh[5];
	word* fint[5];
	init(finh, fint, 5);
	int i = 1, c, d = 0, edit_dist, ind = 0, br = 0;
	char output[10][32];
	char check[33];
	int flag = 0, flag1 = 0, flag2 = 0;
	char* receive;
	strcpy(cur, x);
	if(cur[0] >= 'A' && cur[0] <= 'Z')
		flag2 = 1;
	for(c = 1; c < 33; c++) {
		strcpy(check, findWords(hth, htt, plist, c));
		while(strcmp(check, "") != 0) {
			edit_dist = DL_ed(cur, check);
			if(edit_dist == 0) {
				flag = 1;
				strcpy(op, x);
			}
			else if(edit_dist <= 4) {
				flag1 = 1;
				if(cur[0] == check[0])
					insertPriority(suggh, suggt, edit_dist, check);
				else
					insertSuggestion(suggh, suggt, edit_dist, check);
			}
			strcpy(check, findWords(hth, htt, plist, c));
		}
		findWords(hth, htt, plist, -1);
	}
	d = 0;
	i = 0;
	if(flag1 == 1 && flag == 0) {
		while(i < 5) {
			strcpy(check, findWords(suggh, suggt, &psugg, i + 1));
			while(strcmp(check, "") != 0) {
				ind = arrange(cur, check);
				if(ind < 5) 
					insertSuggestion(finh, fint, ind, check);
				strcpy(check, findWords(suggh, suggt, &psugg, i + 1));
			}
			findWords(suggh, suggt, &psugg, -1);
			i++;
		}
		i = 0;
		while(i < 5) {
			strcpy(check, findWords(finh, fint, &pfin, i + 1));
			while(strcmp(check, "") != 0) {
				if((check[0] >= 'a' && check[0] <= 'z') && flag2 == 1)
					check[0] -= ('a' - 'A');
				strcpy(output[d], check);
				d++;
				if(d == 10) {
					br = 1;
					break;
				}
				strcpy(check, findWords(finh, fint, &pfin, i + 1));
			}
			findWords(finh, fint, &pfin, -1);
			if(br == 1)
				break;
			i++;
		}
		receive = interface(hth, htt, fp1, output, r, cur, d);
		strcpy(op, receive);
		free(receive);
	}
	freeTable(suggh, suggt, &psugg, 5);
	freeTable(finh, fint, &pfin, 3);
}
int main(int argc, char* argv[]) {
	FILE *fp, *fd, *fp1;
	word* hth[32];
	word* htt[32];
	word* plist;
	char ch;
	char ret[33];
	char test[128];
	int j = 0, p = 0, len;
	r = 0;
	if(argc == 3 || argc == 2) {
		if((argc == 3 && strcmp(argv[1], "-c") != 0) || (argc == 2 && strcmp(argv[1], "-a") != 0)) {
			printf("command not found\n");
			exit(0);
		}
	}
	else {
		printf("command not found\n");
		exit(0);
	}
	if(argc == 3) {
		fp = fopen(argv[2], "r");
		if(fp == NULL) {
			printf("Unable to open file\n");
			exit(0);
		}
		fp1 = fopen(argv[2], "r");
		fd = fopen(filename, "a");
		plist = NULL;
		init(hth, htt, 32);
		createTable(hth, htt);
		strcpy(ret, "");
		while(fread(&ch, 1, 1, fp)) {
			if((ch < 'A' || ch > 'Z') && (ch < 'a' || ch > 'z') && (ch != '\'')) {
				if(strlen(ret) != 0) {
					ret[j] = '\0';
					fileop(fp1, hth, htt, &plist, ret);
					fwrite(op, strlen(op), 1, fd);
					r += strlen(ret);
					strcpy(ret, "");
					j = 0;
					p = 0;
				}
				fwrite(&ch, 1, 1, fd);
				r++;
			}	
			else { 
				ret[j] = ch;
				j++;
			}
			
		}
		fclose(fp);
		fclose(fd);
		fp = fopen(argv[2], "w");
		fd = fopen(filename, "r");
		while(fread(&ch, 1, 1, fd))
			fwrite(&ch, 1, 1, fp);
		fclose(fp);
		fclose(fd);
		remove(filename);
		for(j = 1; j < 32; j++)
			removeTemp(hth, htt, j);
	}
	else if(argc == 2) {
		while(1) {
			j = 0;
			ch = getchar();
			while(ch != '\n') {
				test[j++] = ch;
				ch = getchar();
			}
			if(j != 0) {
				test[j] = '\0';
				suggestions(test);
			}
			len = strlen(test);
			for(p = 0; p < len; p++) {
				test[p] = '\0';
			}
		}
	}
	return 0;
}
