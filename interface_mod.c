#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "hashtable.h"
#include "replaceall.h"
#include <sys/ioctl.h>
#include <ctype.h>
#define RED  "\x1B[31m" 
#define RESET "\033[0m"
#define filename "aspbak123.txt"
struct termios raw;
void disableRaw() {
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}
void rawMode() {
	  	tcgetattr(STDIN_FILENO, &raw);
	  	atexit(disableRaw);
	  	struct termios orig = raw;
	  	orig.c_lflag &= ~(ECHO | ICANON);
	  	tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig);
}
char* interface(word** hth, word** htt, FILE* fp1, char output[10][32], int r, char* cur, int d) {
	write(STDOUT_FILENO, "\x1b[2J", 4);
  	write(STDOUT_FILENO, "\x1b[H", 3);
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	int winsiz = w.ws_col;
	char print[10][winsiz];
	int p = 0, u = 0, flag = 0, c, pos, j, g = 0;
	int resume = 0;
	char sp, option, ch;
	char line[128];
	char temp1[33];
	static int prevprinted = 0;
	static int printed = 0;
	static char temp[33];
	static data extra[10];
	static int eind = 0;
	int i;
	for(i = 0; i < eind; i++) 
		if(strcmp(extra[i].str, cur) == 0) 
			return strdup(extra[i].str1);
	if(r > printed) {
		prevprinted = printed;
		g = 1;
	}
	for(i = 0; i < 10; i++) 
		for(j = 0; j < winsiz; j++) 
			print[i][j] = '\0';
	fseek(fp1, prevprinted, SEEK_SET);
	for(i = 0; i < 10; i++) {
		flag = 0;
		for(c = 0; c < strlen(temp); c++) {
			print[i][c] = temp[c];
		}
		for(c = 0; c < 33; c++) {
			temp[c] = '\0';
			temp1[c] = '\0';
		}
		for(j = resume; j < winsiz - 1; j++) {
			if(fread(&sp, 1, 1, fp1)) {
				if(sp == '\n') {
					if(g == 1)
						printed++;
					break;
				}
				else
					print[i][j] = sp;
			}
			else {
				flag = 1;
				i++;
				break;
			}
		}
		resume = 0;	
		if(flag == 1)
		 	break;
		if(fread(&sp, 1, 1, fp1)) {
			if(sp != ' ' && isalpha(print[i][winsiz - 2])) {
				c = 0;
				pos = winsiz - 2;
				while(print[i][pos] != ' ') {
					temp1[c++] = print[i][pos--];
					resume++;
				}
				u = 0;
				for(j = c - 1; j >= 0; j--)
					temp[u++] = temp1[j];
				temp[resume++] = sp;
			}
			else {
				print[i + 1][0] = sp;
				resume++;
			}
		}
		else {
			i++;
			break;
		}
		print[i][winsiz - 1 - resume] = '\0';
	}
	if(g == 1) {
		for(j = 0; j < i; j++) 
			printed += strlen(print[j]);
	}
	for(i = 0; i < 10; i++) {
		for(j = 0; j < strlen(print[i]); j++) {
			if(p >= r - prevprinted && p < r - prevprinted + strlen(cur))
			printf("%s%c%s", RED, print[i][j], RESET); //PRINT COLOURED TEXT
		else
			printf("%c", print[i][j]);
		p++;
		}
		p++; //MAKESHIFT SOLUTION
		printf("\n");
	}
	printf("\n");
	p = 0;
	while(p < d) {
		if((p + 1) % 2 != 0)
			printf("%d.  %s\t\t\t", (p + 1) % 10, output[p]);
		else
			printf("%d.  %s\n", (p + 1) % 10, output[p]);
		p++;
	}
	if(d % 2 != 0)
		printf("\n");
	printf("i.  Ignore\t\t\tI.  Ignore All\nr.  Replace\t\t\tR.  Replace All\n?\n");
	rawMode();
	while(1) {
		read(STDIN_FILENO, &option, 1);
		if((option > '0' && option < '9') || option == 'i' || option == 'I' || option == 'r' || option  == 'R' || option == 'q')
			break;
	}
	if(isdigit(option)) {
		disableRaw();
		return strdup(output[atoi(&option) - 1]); 
	}
	else if(option == 'i') {
		disableRaw();
		return strdup(cur);
	}
	else if(option == 'I') {
		insertTemp(hth, htt, cur);
		disableRaw();
		return strdup(cur);
	}
	else if(option == 'r') {
		u = 0;
		printf("Enter the replacement: ");
		disableRaw();
		ch = getchar();
		while(ch != '\n') {
			line[u++] = ch;
			ch = getchar();
		}
		line[u] = '\0';
		return strdup(line);
	}
	else if(option == 'R') {
		u = 0;
		printf("Enter the replacement: ");
		disableRaw();
		ch = getchar();
		while(ch != '\n') {
			line[u++] = ch;
			ch = getchar();
		}
		line[u] = '\0';
		extra[eind].str = (char*) malloc(strlen(cur) + 1);
		extra[eind].str1 = (char*) malloc(strlen(line) + 1);
		strcpy(extra[eind].str, cur);
		strcpy(extra[eind].str1, line);
		eind++;
		return strdup(line);
	}
	else if(option == 'q') {
		remove(filename);
		exit(0);
	}
}
