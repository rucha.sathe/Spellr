#include "hashtable.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
void init(word** hth, word** htt, int size) { 
	int i = 0;
	while(i < size) {
		hth[i] = NULL;
		htt[i] = NULL;
		i++;
	}
}
void insertWord(word** hth, word** htt, char* entry) {
	int i = strlen(entry) - 1;
	word* tmp;
	tmp = (word*) malloc(sizeof(word));
	tmp->data = (char*) malloc(i + 2);
	strcpy(tmp->data, entry);
	tmp->next = NULL;
	if(hth[i]) {
		htt[i]->next = tmp;
		tmp->prev = htt[i];
	}
	else {
		hth[i] = tmp;
		tmp->prev = NULL;
	}
	htt[i] = tmp;
}
char* findWords(word** hth, word** htt, word** count, int length) { 
	if(length == -1) {
		*count = NULL;
		return "";
	}
	else if(*count == NULL) {
		*count = hth[length - 1];
		if(*count)
			return (*count)->data;
		else
			return "";
	}
	else {
		*count = (*count)->next;
		if(*count)
			return (*count)->data;
		else
			return "";
	}
}
void insertSuggestion(word** hth, word** htt,  int ed, char* entry) { 
	word* tmp;
	tmp = (word*) malloc(sizeof(word));
	tmp->data = (char*) malloc(strlen(entry) + 1);
	strcpy(tmp->data, entry);
	tmp->next = NULL;
	if(hth[ed]) {
		htt[ed]->next = tmp;
		tmp->prev = htt[ed];
	}
	else {
		hth[ed] = tmp;
		tmp->prev = NULL;
	}
	htt[ed] = tmp;
}
void insertTemp(word** hth, word** htt, char* add) {
	int i = strlen(add) - 1;
	word* tmp;
	tmp = (word*) malloc(sizeof(word));
	tmp->data = (char*) malloc(i + 2);
	strcpy(tmp->data, add);
	tmp->next = NULL;
	if(hth[i]) {
		htt[i]->next = tmp;
		tmp->prev = htt[i];
	}
	else {
		hth[i] = tmp;
		tmp->prev = NULL;
	}
}
void removeTemp(word** hth, word** htt, int ind) {
	word* tmp = htt[ind - 1];
	if(tmp == NULL || tmp->next == NULL)
		return;
	while(tmp->next->next != NULL) 
		tmp = tmp->next;
	while(tmp != htt[ind - 1]->prev) {
		free(tmp->next);
		tmp = tmp->prev;
	}
	htt[ind - 1]->next = NULL;
}
void insertPriority(word** hth, word** htt, int ed, char* entry) {
	word* tmp;
	tmp = (word*) malloc(sizeof(word));
	tmp->data = (char*) malloc(strlen(entry) + 1);
	strcpy(tmp->data, entry);
	tmp->prev = NULL;
	if(hth[ed] == NULL) {
		hth[ed] = tmp;
		tmp->next = NULL;
		htt[ed] = tmp;
		return;
	}
	hth[ed]->prev = tmp;
	tmp->next = hth[ed];
	hth[ed] = tmp;
}
void freeTable(word** hth, word** htt, word** count, int size) {
	int i = 0;
	*count = NULL;
	while(i < size) {
		if(hth[i] == NULL) {
			i++;
			continue;
		}
		while(htt[i]->prev) {
			htt[i] = htt[i]->prev;
			free(htt[i]->next);
		}
		free(hth[i]);
		htt[i] = NULL;
		hth[i] = NULL;
		i++;
	}
}
