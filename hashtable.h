#define MAX 32
typedef struct word {
	char* data;
	struct word* next;
	struct word* prev;
} word;
void init(word** hth, word** htt, int size);
void insertWord(word** hth, word** htt, char* entry);
char* findWords(word** hth, word** htt, word** count, int length);
void insertSuggestion(word** hth, word** htt, int ed, char* entry);
void freeTable(word** hth, word** htt, word** count, int size);
void insertPriority(word** hth, word** htt, int ed, char* entry);
void insertTemp(word** hth, word** htt, char* add);
void removeTemp(word** hth, word** htt, int ind);
