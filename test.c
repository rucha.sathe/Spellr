#include <stdio.h>
#include "hashtable.h"
#include "DL.h"
#include "suggestions.h"
#include "create.h"
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
char* split(char* entry) {
	static char* tmp;
	static int i = 0;
	int j = 0;
	char ret[33];
	if(entry) {
		tmp = entry;
		i = 0;
	}
	while(tmp[i] !=  ' ' && tmp[i] !=  '\t' && tmp[i] !=  '\n' && tmp[i] != '.' && tmp[i] != ',' && tmp[i] != '?') {
		if(tmp[i] == '\0') {
			if(j == 0) 
				return NULL;
			else
				break;
		}
		ret[j] = tmp[i];
		i++;
		j++;
	}
	ret[j] = '\0';
	i++;
	while(tmp[i] ==  ' ' || tmp[i] ==  '\t' || tmp[i] ==  '\n' || tmp[i] == '.' || tmp[i] == ',' || tmp[i] == '?' || tmp[i] == ';' || tmp[i] == ':' || tmp[i] == '!') 
		i++;
	return strdup(ret);
}
void suggestions(char* test) {
	word* pfin = NULL;
	word* plist = NULL;
	word* psugg = NULL;
	word* hth[32];
	word* htt[32];
	init(hth, htt, 32);
	createTable(hth, htt);
	char cur[33];
	word* suggh[5];
	word* suggt[5];
	init(suggh, suggt, 5);
	word* finh[5];
	word* fint[5];
	init(finh, fint, 5);
	int i = 1, c, d;
	char output[20][32];
	char check[33];
	char* x;
	int edit_dist, ind = 0, br = 0, m = 0;
	int flag = 0, flag1 = 0, flag2 = 0;
	x = split(test);
	while(x != NULL) {
		flag2 = 0;
		strcpy(cur, x);
		free(x);
		if(cur[0] >= 'A' && cur[0] <= 'Z')
			flag2 = 1;
		d = 0;
		for(c = 1; c < 33; c++) {
			strcpy(check, findWords(hth, htt, &plist, c));
			while(strcmp(check, "") != 0) {
				edit_dist = DL_ed(cur, check);
				if(edit_dist == 0) {
					printf("*\n");
					flag = 1;
					break;
				}
				else if(edit_dist <= 4) {
					flag1 = 1;
					insertSuggestion(suggh, suggt, edit_dist, check);
				}
				strcpy(check, findWords(hth, htt, &plist, c));
			}
			findWords(hth, htt, &plist, -1);
		}
		d = 0;
		i = 0;
		if(flag == 0 && flag1 == 1) {
			while(i < 5) {
				strcpy(check, findWords(suggh, suggt, &psugg, i + 1));
				while(strcmp(check, "") != 0) {
					ind = arrange(cur, check);
					if(ind < 5) 
						insertSuggestion(finh, fint, ind, check);
					strcpy(check, findWords(suggh, suggt, &psugg, i + 1));
				}
				findWords(suggh, suggt, &psugg, -1);
				i++;
			}
			i = 0;
			while(i < 5) {
				strcpy(check, findWords(finh, fint, &pfin, i + 1));
				while(strcmp(check, "") != 0) {
					if((check[0] >= 'a' && check[0] <= 'z') && flag2 == 1)
						check[0] -= ('a' - 'A');
					strcpy(output[d], check);
					d++;
					if(d == 20) {
						br = 1;
						break;
					}
					strcpy(check, findWords(finh, fint, &pfin, i + 1));
				}
				findWords(finh, fint, &pfin, -1);
				if(br == 1)
					break;
				i++;
			}
			m = 0;
			if(d > 0) {
				printf("& %s %d %d: %s", cur, d, 0, output[m++]);
				while(m < d) {
					printf(", %s", output[m]);
					m++;
				}
			}
			else
				printf("# %s %d %d", cur, d, 0);
		}
		printf("\n\n");
		freeTable(suggh, suggt, &psugg, 5);
		freeTable(finh, fint, &pfin, 3);
		x = split(NULL);
		i = 1;
		flag = 0;
		flag1 = 0;
		br = 0;
	}
}
