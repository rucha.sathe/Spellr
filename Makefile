project: Damerau-Levenshtein.o main.o arrange.o create.o hashtable.o test.o interface_mod.o
	cc Damerau-Levenshtein.o main.o arrange.o create.o hashtable.o test.o interface_mod.o -o project
Damerau-Levenshtein.o: Damerau-Levenshtein.c
	cc -c Damerau-Levenshtein.c -Wall
main.o: main.c hashtable.h DL.h suggestions.h create.h interface.h
	cc -c main.c -Wall
arrange.o: arrange.c DL.h
	cc -c arrange.c -Wall
create.o: create.c hashtable.h 
	cc -c create.c -Wall
hashtable.o: hashtable.c hashtable.h
	cc -c hashtable.c -Wall
test.o: test.c DL.h hashtable.h create.h
	cc -c test.c -Wall
interface_mod.o: interface_mod.c hashtable.h replaceall.h
	cc -c interface_mod.c -Wall
clean: rm *.o
