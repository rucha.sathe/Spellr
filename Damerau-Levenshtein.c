/* To calculate the score of the word being compared to input word by Damerau - Levenshtein formula.
*  Here s1 is the input string, s2 is the string being converted to input string.
*  The function DL_ed() returns the score of the word according to how many transformations
*  (insertions, deletions, replacements and swaps) are made and according to the heirarchy of each of these
*  trasformations.
*  Weights have been taken into consideration.
*  To convert look to like: l -> l : 1 * 0 = 0, i -> o : 1 * subs = 1, o -> delete : 1 * insdel = 1, add -> e : 1 * insdel = 1.
*  Total score = 3.
*  Damerau - Levenshtein distance : https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
int min3(int a, int b, int c) {
	return (a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c);
}
int min2(int a, int b) {
	return a < b ? a : b;
}
int max2(int a, int b) {
	return a > b ? a : b;
}
void fix(char* s) {
	int i = 0;
	while(i < strlen(s)) {
		if(s[i] >= 'A' && s[i] <= 'Z')
			s[i] += ('a' - 'A');
		i++;
	}
}
int DL_ed(char * s1t, char * s2t) {
	char* s1; 
	char* s2; 
	s1 = strdup(s1t);
	s2 = strdup(s2t);
	fix(s1);
	fix(s2);
	int insdel = 1, subs = 1, ex = 1;		//WEIGHTS OF EACH TRANSFORMATION
	int s1len = strlen(s1);
	int s2len = strlen(s2);
	int i, j, cost_s, cost_e;
	int matrix[s1len + 1][s2len + 1];
	for(i = 0; i <= s1len; i++)
		matrix[i][0] = i * insdel;
	for(j = 0; j <= s2len; j++)
		matrix[0][j] = j * insdel;
	for(i = 1; i <= s1len; i++) {
		for(j = 1; j <= s2len; j++) {
			if(s1[i - 1] == s2[j - 1]) {
				cost_s = 0;
				cost_e = 0;
				matrix[i][j] = matrix[i - 1][j - 1];
			}
			else {
				cost_s = subs;
				cost_e = ex;
				matrix[i][j] = min3((matrix[i][j - 1] + insdel), (matrix[i - 1][j] + insdel), (matrix[i - 1][j - 1] + cost_s));
				if(i > 1 && j > 1 && s1[i - 2] == s2[j - 1] && s1[i - 1] == s2[j - 2])
					matrix[i][j] = min2(matrix[i][j], (matrix[i - 2][j - 2] + cost_e));
			}
		}
	}
	free(s1);
	free(s2);
	return matrix[s1len][s2len]; 
}
