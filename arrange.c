#include <stdio.h>
#include "hashtable.h"
#include "DL.h"
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
/* To arrange the words that have been shortlisted up till this point in a more accurate order based on the sequence of characters
*  in the input misspelled word.
*  Priority is assigned based on the number of common characters in the suggestion and the misspelled word.
*  The difference in length and the starting character is also taken into consideration for priority assignment.
*  This function receives the two strings(misspelled word, suggestion) to be compared as input.
*  The output is an integer indicating the index at which the suggestion must be inserted in the new and improved hashtable.
*/
int arrange(char* s1t, char* s2t) {
	char* s1; 
	char* s2; 
	s1 = strdup(s1t);
	s2 = strdup(s2t);
	fix(s1);
	fix(s2);
	int s1len = strlen(s1);
	int s2len = strlen(s2);
	int s1c[26], s2c[26], cnt = 0, r, i, j; 
	for(r = 0; r < 26; r++) {
		s1c[r] = 0;
		s2c[r] = 0;
	}
	i = 0;
	j = 0;
	while(i < s1len) {
		r = s1[i] - 97;
		s1c[r]++;
		i++;
	}
	while(j < s2len) {
		r = s2[j] - 97;
		s2c[r]++;
		j++;
	}
	i = 0;
	while(i < s1len) {
		r = s1[i] - 97;
		if(s1c[r] > 0 && s2c[r] > 0) {
			cnt++;
			s1c[r]--;
			s2c[r]--;
		}
		i++;
	}
	if(s1[0] == s2[0] && s1len == s2len)
		cnt += 1;
	free(s1);
	free(s2);
	return (s1len - cnt + 1);
}
