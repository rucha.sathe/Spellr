Spellr (Aspell like Spellchecker)

-Rucha Sathe, 111603054

Spellr checks the user's input / text written in a file, one word at a time for spelling errors and suggests similar words that the user might have meant to type originally, but ended up mistyping. It's a primitive version of Aspell.

It also has options such as Replace, Replace All, Ignore and Ignore All which replace the current incorrect word / all its occurrences by a user entered word or ignore the current incorrect word / all its occurrences in the rest of the input.

Usage:
To spellcheck a file:
./project -c <filename>

To spellcheck a string:
./project -a
